var elixir = require('laravel-elixir');
require('laravel-elixir-remove');

elixir(function(mix) {  
    /**
     * App
     */
        // Compile sass
        mix.sass('./src/assets/sass/app.scss', './src/assets/bundle.css');

        // Compile scripts
        mix.scripts([
            // "./node_modules/jquery/dist/jquery.js",
            // "./resources/assets/js/app.js",
            "./node_modules/bootstrap/dist/js/bootstrap.js",
        ], './src/assets/bundle.js');

        // Version
        mix.remove('src/rev-manifest.json');
        mix.remove('src/bundle-*.*');
        if (elixir.config.production) {
            mix.version([
                'src/bundle.css',
                'src/bundle.js'
            ], 'public');
            mix.remove('src/bundle.*');
        }

            
});