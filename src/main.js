import Vue from 'vue'
import App from './App.vue'
import router from './router'
import axios from 'axios'  
import VueAxios from 'vue-axios'
require('@/assets/bundle.css')

Vue.config.productionTip = false
Vue.use(VueAxios, axios)
 

new Vue({
  router,
  axios,
  VueAxios,
  render: h => h(App)
}).$mount('#app')
router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (!localStorage.getItem('token')) {
      next({
        path: '/',
      })
    } else {
        next()
    }
  } else {
    next();
  }
}) 