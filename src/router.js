import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Offers from './views/Offers.vue'
import OfferSingle from './views/Offer-single.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home,
    },
    {
      path: '/offer/:id',
      props: true,
      name: 'Offer',
      component: OfferSingle,
    },
    {
      path: '/offers',
      name: 'Offers',
      component: Offers,
    },
  ]
})